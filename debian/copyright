Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: hovercraft
Upstream-Contact: Lennart Regebro <regebro@gmail.com>
Source: https://github.com/regebro/hovercraft

Files: *
Copyright: 2013-2016 Lennart Regebro <regebro@gmail.com>
                     Fahrzin Hemmati <fahhem@gmail.com>
                     Christophe Labouisse <consulting@labouisse.com>
                     Carl Meyer <carl@oddbird.net>
                     Chris Withers <chris@simplistix.co.uk>
License: Expat

Files: debian/*
Copyright: 2015-2016 Daniel Stender <stender@debian.org>
License: Expat

Files: hovercraft/templates/default/js/impress.js
       hovercraft/templates/simple/js/impress.js
       tests/test_data/maximal/js/impress.js
       tests/test_data/minimal/js/impress.js
Copyright: 2011-2012 Bartek Szopka
License: Expat or GPL-2+
Comment: https://github.com/bartaz/impress.js/issues/225

Files: tests/test_data/maximal/css/impressConsole.css
       tests/test_data/maximal/js/impressConsole.js
Copyright: 2012-2013 Lennart Regebro
                     David Souther <davidsouther@gmail.com>
License: Expat
Comment: https://github.com/regebro/impress-console

Files: hovercraft/templates/reST.xsl
Copyright: 2006 Michael Alyn Miller <malyn@strangeGizmo.com>
License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
 1. Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 3. Neither the name of the author nor the names of its contributors may
 be used to endorse or promote products derived from this software
 without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: tests/test_data/maximal/fonts/texgyreschola-regular-webfont*
Copyright: 2007-2009 B. Jackowski
                     J. M. Nowacki
Comment: http://www.gust.org.pl/projects/e-foundry/tex-gyre/schola/readme-tex-gyre-schola.txt/view
License: GFL
 This is version 1.0, dated 22 June 2009, of the GUST Font License.
 (GUST is the Polish TeX Users Group, http://www.gust.org.pl)
 .
 For the most recent version of this license see
 http://www.gust.org.pl/fonts/licenses/GUST-FONT-LICENSE.txt or
 http://tug.org/fonts/licenses/GUST-FONT-LICENSE.txt
 .
 This work may be distributed and/or modified under the conditions
 of the LaTeX Project Public License, either version 1.3c of this
 license or (at your option) any later version.
 .
 Please also observe the following clause:
 1) it is requested, but not legally required, that derived works be
 distributed only after changing the names of the fonts comprising this
 work and given in an accompanying "manifest", and that the
 files comprising the Work, as listed in the manifest, also be given
 new names. Any exceptions to this request are also given in the
 manifest.
 .
 We recommend the manifest be given in a separate file named
 MANIFEST-<fontid>.txt, where <fontid> is some unique identification
 of the font family. If a separate "readme" file accompanies the Work,
 we recommend a name of the form README-<fontid>.txt.
 .
 The latest version of the LaTeX Project Public License is in
 http://www.latex-project.org/lppl.txt and version 1.3c or later
 is part of all distributions of LaTeX version 2006/05/20 or later.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License: GPL-2+
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General Public License
 version 2 can be found in the /usr/share/common-licenses/GPL-2 file.
